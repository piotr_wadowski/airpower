//
//  DataAccessAPITests.swift
//  AirPowerTests
//
//  Created by Piotr Wadowski on 08.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import XCTest
@testable import AirPower

class DataAccessAPITests: XCTestCase {
    
    var access: DataAccessAPI!
    
    override func setUp() {
        super.setUp()
        
        access = DataAccessAPI()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetAllStations() {
        //Given
        let expect = expectation(description: "Downloading stations")
        let numberOfStations = 158
        
        //When
        access.getAllStations { (stations) in
            if stations.count == numberOfStations {
                expect.fulfill()
            } else {
                XCTFail()
            }
        }
        
        //Then
        wait(for: [expect], timeout: 20.0)
    }
    
    func testAddSensorsToStation() {
        //Given
        var expect = expectation(description: "Downloading stations")
        let dbAccess = DataAccessLocal()
        var station: Station!
        access.getAllStations { (stations) in
            dbAccess.addStations(stations)
            station = stations.first
            expect.fulfill()
        }
        wait(for: [expect], timeout: 20.0)
        expect = expectation(description: "Downloading sensors")
        
        //When
        access.getSensorsForStation(station) { (sensors) in
            dbAccess.addSensorsToStation(station, sensors: sensors)
            expect.fulfill()
        }
        
        //Then
        wait(for: [expect], timeout: 20.0)
        XCTAssert(!station.sensors.isEmpty)
    }
    
    func testGetValueFromSensor() {
        //Given
        var expect = expectation(description: "Downloading sensors")
        let dbAccess = DataAccessLocal()
        var sensor: Sensor!
        access.getAllStations { (stations) in
            let testStation = stations.first!
            dbAccess.addStations([testStation])
            self.access.getSensorsForStation(testStation, completion: { (sensors) in
                sensor = sensors.first
                dbAccess.addSensorsToStation(testStation, sensors: [sensor])
                expect.fulfill()
            })
        }
        wait(for: [expect], timeout: 20.0)
        expect = expectation(description: "Downloading value for sensor")
        
        //When
        access.getActualValueForSensor(sensor) { (resp) in
            if let (val, date) = resp {
                print("\(val) : \(date)")
                expect.fulfill()
            } else {
                XCTFail()
            }
        }
        
        //Then
        wait(for: [expect], timeout: 20.0)
    }
    
    func testGetAQIForStation() {
        //Given
        var expect = expectation(description: "Downloading stations")
        var station: Station!
        access.getAllStations { (stations) in
            station = stations.first
            expect.fulfill()
        }
        wait(for: [expect], timeout: 20.0)
        expect = expectation(description: "Downloading AQI")
        var testAQI: AQILevel!
        
        //When
        access.getAQIForStation(station) { (resp) in
            if let (aqi, _) = resp {
                testAQI = aqi
                expect.fulfill()
            } else {
                XCTFail()
            }
        }
        
        //Then
        wait(for: [expect], timeout: 20.0)
        XCTAssert(testAQI != nil)
        print(testAQI)
    }
    
}
