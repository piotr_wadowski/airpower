//
//  DataAccessLocalTests.swift
//  AirPowerTests
//
//  Created by Piotr Wadowski on 08.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import XCTest
@testable import AirPower
import RealmSwift

class DataAccessLocalTests: XCTestCase {

    let db = try! Realm()
    var access: DataAccessLocal!
    
    var testStations = [
        Station(id: 1, name: "aStation", latitude: 1.0, longitude: 1.0),
        Station(id: 2, name: "bStation", latitude: 2.0, longitude: 2.0),
        Station(id: 3, name: "cStation", latitude: 3.0, longitude: 3.0)
    ]
    
    override func setUp() {
        super.setUp()
        
        access = DataAccessLocal()
    }
    
    override func tearDown() {
        try! db.write {
            db.deleteAll()
        }
        testStations = [
            Station(id: 1, name: "aStation", latitude: 1.0, longitude: 1.0),
            Station(id: 2, name: "bStation", latitude: 2.0, longitude: 2.0),
            Station(id: 3, name: "cStation", latitude: 3.0, longitude: 3.0)
        ]
        
        super.tearDown()
    }
    
    func testAddStationsEmptyDB() {
        //Given
        let sensor = Sensor(id: 1, code: "PM10")
        let station = Station(id: 1, name: "aStation", latitude: 1.0, longitude: 1.0, sensors: [sensor])
        
        //When
        access.addStations([station])
        
        //Then
        XCTAssertEqual([station], Array(db.objects(Station.self)))
        XCTAssertEqual([sensor], Array(db.objects(Sensor.self)))
    }
    
    func testAddStationsChangeStations() {
        //Given
        let oldSensor = Sensor(id: 1, code: "PM10")
        let oldStation = Station(id: 1, name: "aStation", latitude: 1.0, longitude: 1.0, sensors: [oldSensor])
        try! db.write {
            db.add(oldStation)
        }
        let newSensor = Sensor(id: 2, code: "NO")
        let newStation = Station(id: 2, name: "bStation", latitude: 2.0, longitude: 2.0, sensors: [newSensor])
        
        //When
        access.addStations([newStation])
        
        //Then
        XCTAssert(!db.objects(Station.self).contains(oldStation), "Old station isn't deleted")
        XCTAssert(!db.objects(Sensor.self).contains(oldSensor), "Old sensor isn't deleted")
        XCTAssert(db.objects(Station.self).contains(newStation))
        XCTAssert(db.objects(Sensor.self).contains(newSensor))
    }
    
    func testSetChosen() {
        //Given
        let station = testStations[0]
        addTestStations()
        
        //When
        access.setChosen(station: station, chosen: true)
        
        //Then
        let chosenStations = db.objects(Station.self).filter("isChosen == true AND id == \(station.id)")
        XCTAssert(!chosenStations.isEmpty)
    }
    
    func testSetFavourite() {
        //Given
        let station = testStations[0]
        addTestStations()
        
        //When
        access.setFavourite(station: station, favourite: true)
        
        //Then
        let favStations = db.objects(Station.self).filter("isFavourite == true AND id == \(station.id)")
        XCTAssert(!favStations.isEmpty)
    }
    
    func testSetFavouriteSecond() {
        //Given
        let station1 = testStations[0]
        station1.isFavourite = true
        let station2 = testStations[1]
        addTestStations()
        
        //When
        access.setFavourite(station: station2, favourite: true)
        
        //Then
        let favStations = db.objects(Station.self).filter("isFavourite == true")
        XCTAssert(!favStations.isEmpty)
        XCTAssertEqual(1, favStations.count)
        XCTAssertEqual(station2, favStations.first!)
    }
    
    func testAllStations() {
        //Given
        addTestStations()
        
        //When
        let stations = access.allStations
        
        //Then
        XCTAssertEqual(testStations, stations)
    }
    
    func testChosenStations() {
        //Given
        let station = testStations[0]
        station.isChosen = true
        addTestStations()
        
        //When
        let stationsFromDb = access.chosenStations
        
        //Then
        XCTAssertEqual([station], stationsFromDb)
    }
    
    func testFavStation() {
        //Given
        let station = testStations[0]
        station.isFavourite = true
        addTestStations()
        
        //When
        let favStation = access.favStation
        
        //Then
        XCTAssertEqual(station, favStation)
    }
    
    func testFavStationNoFavStation() {
        //Given
        addTestStations()
        
        //When
        let favStation = access.favStation
        
        //Then
        XCTAssertNil(favStation)
    }
    
    private func addTestStations() {
        try! db.write {
            db.add(testStations)
        }
    }
}
