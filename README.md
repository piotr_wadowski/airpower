#  AirPower

### Author
Piotr Wadowski

### Description
Simple app showing level of air pollution in Poland.

### Dependencies
- RealmSwift
- Alamofire


All graphics are from [Icons8](https://icons8.com).
Special thanks to RayWenderlich.com for their Core Graphics tutorial series, where they show how to create gauge control.
