//
//  Extensions.swift
//  AirPower
//
//  Created by Piotr Wadowski on 10.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func drawGradient(startColor: UIColor, endColor: UIColor, startPoint: CGPoint, endPoint: CGPoint) {
        let colors = [startColor.cgColor, endColor.cgColor]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocations: [CGFloat] = [0.0, 1.0]
        let gradient = CGGradient(colorsSpace: colorSpace, colors: colors as CFArray, locations: colorLocations)
        let context = UIGraphicsGetCurrentContext()
        context!.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: [])
    }
    
}

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return Theme.current.statusBarStyle
    }
    
}

extension UIColor {
    
    convenience init?(hex hexStr: String, alpha: CGFloat) {
        let startIndex = hexStr.startIndex
        guard hexStr[startIndex] == "#" && hexStr.count == 7 else {
            return nil
        }
        if let hex = Int(hexStr[hexStr.index(after: startIndex)...], radix: 16) {
            let red = CGFloat(hex >> 16) / 0xff
            let green = CGFloat((hex & 0x00ff00) >> 8) / 0xff
            let blue = CGFloat((hex & 0x0000ff)) / 0xff
            self.init(red: red, green: green, blue: blue, alpha: alpha)
        } else {
            return nil
        }
    }
    
}
