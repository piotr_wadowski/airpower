//
//  Theme.swift
//  AirPower
//
//  Created by Piotr Wadowski on 10.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit

enum Theme: Int {
    case standard
    
    private struct Constants {
        static let savedTheme = "savedTheme"
    }
    
    static var current: Theme {
        let savedTheme = UserDefaults.standard.integer(forKey: Constants.savedTheme)
        return Theme(rawValue: savedTheme) ?? .standard
    }
    
    var statusBarStyle: UIStatusBarStyle {
        switch self {
        case .standard:
            return .lightContent
        }
    }
    
    var logoText: NSAttributedString {
        let text = NSAttributedString(string: "AirPower", attributes: [
            NSAttributedStringKey.font: UIFont(name: "Baskerville-SemiBoldItalic", size: 30.0)!,
            NSAttributedStringKey.foregroundColor: UIColor.white
            ])
        return text
    }
    
    var mainColor: UIColor {
        switch self {
        case .standard:
            return UIColor(hex: "#0099ff", alpha: 1.0)!
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .standard:
            return UIColor(hex: "#f8f8f8", alpha: 1.0)!
        }
    }
    
    var itemsColor: UIColor {
        switch self {
        case .standard:
            return UIColor.white
        }
    }
    
    var infoLabelFont: UIFont {
        switch self {
        case .standard:
            return UIFont.systemFont(ofSize: 20.0)
        }
    }
    
    var buttonsTitleAttributes: [NSAttributedStringKey: Any] {
        switch self {
        case .standard:
            return [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 20.0),
                    NSAttributedStringKey.foregroundColor: UIColor.white]
        }
    }
    
    func apply() {
        UserDefaults.standard.set(rawValue, forKey: Constants.savedTheme)
        
        let navBar = UINavigationBar.appearance()
        navBar.isTranslucent = false
        navBar.barTintColor = mainColor
        navBar.tintColor = itemsColor
        navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : itemsColor]
    }
}
