//
//  DataAccessLocal.swift
//  AirPower
//
//  Created by Piotr Wadowski on 08.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

class DataAccessLocal {
    
    private let db: Realm
    
    public var allStations: [Station] {
        return Array(db.objects(Station.self))
    }
    
    public var chosenStations: [Station] {
        return Array(db.objects(Station.self).filter("isChosen == true"))
    }
    
    public var favStation: Station? {
        return db.objects(Station.self).filter("isFavourite == true").first
    }
    
    public init() {
        self.db = try! Realm()
    }
    
    public func addStations(_ stations: [Station]) {
        do {
            try db.write {
                let stationsToDelete = self.allStations
                stationsToDelete.forEach { db.delete($0.sensors) }
                db.delete(stationsToDelete)
                db.add(stations)
            }
        } catch {
            print("🛑 - " + error.localizedDescription)
            return
        }
    }
    
    public func setChosen(station: Station, chosen: Bool) {
        if checkExistance(station: station) {
            do {
                try db.write {
                    station.isChosen = chosen
                }
            } catch {
                print("🛑 - " + error.localizedDescription)
                return
            }
        }
    }
    
    public func setFavourite(station: Station, favourite: Bool) {
        if checkExistance(station: station) {
            let previousFav = db.objects(Station.self).filter("isFavourite == true AND id != \(station.id)").first
            do {
                try db.write {
                    if let previousFav = previousFav, favourite {
                        previousFav.isFavourite = false
                    }
                    station.isFavourite = favourite
                }
            } catch {
                print("🛑 - " + error.localizedDescription)
                return
            }
        }
    }
    
    public func addSensorsToStation(_ station: Station, sensors: [Sensor]) {
        if checkExistance(station: station) {
            do {
                try db.write {
                    station.sensors.removeAll()
                    station.sensors.append(objectsIn: sensors)
                }
            } catch {
                print("🛑 - " + error.localizedDescription)
                return
            }
        }
    }
    
    public func getClosestStation(fromLocation location: CLLocation) -> Station? {
        var closestStation: Station?
        var lastDistance = Double.infinity
        for station in allStations {
            let dist = station.location.distance(from: location)
            if dist < lastDistance {
                lastDistance = dist
                closestStation = station
            }
        }
        return closestStation
    }
    
    private func checkExistance(station: Station) -> Bool {
        return !db.objects(Station.self).filter("id == \(station.id)").isEmpty
    }
}
