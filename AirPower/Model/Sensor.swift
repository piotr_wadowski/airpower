//
//  Sensor.swift
//  AirPower
//
//  Created by Piotr Wadowski on 08.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import RealmSwift

class Sensor: Object {
    
    @objc private dynamic var key = UUID().uuidString
    
    @objc dynamic var id: Int = -1
    @objc dynamic var code: String = ""
    
    convenience init(id: Int, code: String) {
        self.init()
        self.id = id
        self.code = code
    }
    
    override public static func primaryKey() -> String {
        return "key"
    }
}
