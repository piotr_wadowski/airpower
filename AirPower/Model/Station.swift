//
//  Station.swift
//  AirPower
//
//  Created by Piotr Wadowski on 08.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

class Station: Object {
    
    @objc private dynamic var key = UUID().uuidString
    
    @objc dynamic var id: Int = -1
    @objc dynamic var name: String = ""
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var isFavourite = false
    @objc dynamic var isChosen = false
    let sensors = List<Sensor>()
    
    var location: CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    convenience init(id: Int, name: String, latitude: Double, longitude: Double, sensors: [Sensor] = []) {
        self.init()
        self.id = id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.sensors.append(objectsIn: sensors)
    }
    
    override public static func primaryKey() -> String {
        return "key"
    }
}
