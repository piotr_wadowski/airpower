//
//  AQILevel.swift
//  AirPower
//
//  Created by Piotr Wadowski on 09.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import UIKit

enum AQILevel: String {
    case veryGood = "bardzo dobry"
    case good = "dobry"
    case moderate = "umiarkowany"
    case sufficient = "dostateczny"
    case bad = "zły"
    case veryBad = "bardzo zły"
    
    var unhealhtyPercent: Double {
        switch self {
        case .veryGood:
            return 1/6
        case .good:
            return 2/6
        case .moderate:
            return 3/6
        case .sufficient:
            return 4/6
        case .bad:
            return 5/6
        case .veryBad:
            return 1.0
        }
    }
    
    var name: String {
        switch self {
        case .veryGood:
            return NSLocalizedString("AQI_VGOOD", comment: "Name of very good AQI level")
        case .good:
            return NSLocalizedString("AQI_GOOD", comment: "Name of good AQI level")
        case .moderate:
            return NSLocalizedString("AQI_MODERATE", comment: "Name of moderate AQI level")
        case .sufficient:
            return NSLocalizedString("AQI_SUFFICIENT", comment: "Name of sufficient AQI level")
        case .bad:
            return NSLocalizedString("AQI_BAD", comment: "Name of bad AQI level")
        case .veryBad:
            return NSLocalizedString("AQI_VBAD", comment: "Name of very bad AQI level")
        }
    }
    
    var image: UIImage? {
        switch self {
        case .veryGood:
            return UIImage(named: "veryGood")
        case .good:
            return UIImage(named: "good")
        case .moderate:
            return UIImage(named: "moderate")
        case .sufficient:
            return UIImage(named: "sufficient")
        case .bad:
            return UIImage(named: "bad")
        case .veryBad:
            return UIImage(named: "veryBad")
        }
    }
}
