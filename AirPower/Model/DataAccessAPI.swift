//
//  DataAccessAPI.swift
//  AirPower
//
//  Created by Piotr Wadowski on 08.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation
import Alamofire

class DataAccessAPI {
    
    func getAllStations(completion: @escaping ([Station]) -> ()) {
        let address = "http://api.gios.gov.pl/pjp-api/rest/station/findAll"
        Alamofire.request(address).responseJSON { (response) in
            if let data = response.data, response.error == nil {
                print("✅ - Stations downloaded")
                if let stations = self.decodeStationsResponse(data) {
                    completion(stations)
                }
            } else {
                print("🛑 - " + response.error!.localizedDescription)
                completion([])
            }
        }
    }
    
    func getSensorsForStation(_ station: Station, completion: @escaping ([Sensor]) -> ()) {
        let address = "http://api.gios.gov.pl/pjp-api/rest/station/sensors/\(station.id)"
        Alamofire.request(address).responseJSON { (response) in
            if let data = response.data, response.error == nil {
                print("✅ - Sensors downloaded")
                if let sensors = self.decodeSensorsResponse(data) {
                    completion(sensors)
                }
            } else {
                print("🛑 - " + response.error!.localizedDescription)
                completion([])
            }
        }
    }
    
    func getActualValueForSensor(_ sensor: Sensor, completion: @escaping ((Double, Date)?) -> ()) {
        let address = "http://api.gios.gov.pl/pjp-api/rest/data/getData/\(sensor.id)"
        Alamofire.request(address).responseJSON { (response) in
            if let data = response.data, response.error == nil {
                print("✅ - Data for sensor downloaded")
                if let value = self.decodeSensorValue(data) {
                    completion(value)
                }
            } else {
                print("🛑 - " + response.error!.localizedDescription)
                completion(nil)
            }
        }
    }
    
    func getAQIForStation(_ station: Station, completion: @escaping ((AQILevel, Date)?) -> ()) {
        let address = "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/\(station.id)"
        Alamofire.request(address).responseJSON { (response) in
            if let data = response.data, response.error == nil {
                print("✅ - AQI for station downloaded")
                if let (name, date) = self.decodeAQIName(data), let aqi = AQILevel(rawValue: name) {
                    completion((aqi, date))
                }
            } else {
                print("🛑 - " + response.error!.localizedDescription)
                completion(nil)
            }
        }
    }
    
    private func decodeStationsResponse(_ data: Data) -> [Station]? {
        var result = [Station]()
        var stationsArray: [Any]?
        do {
            stationsArray = try JSONSerialization.jsonObject(with: data, options: []) as? [Any]
        } catch {
            print("🛑 - " + error.localizedDescription)
            return nil
        }
        
        if let stationsArray = stationsArray {
            for stationRaw in stationsArray {
                if let station = stationRaw as? [String: Any] {
                    let id = station["id"] as! Int
                    let name = station["stationName"] as! String
                    let latitude = Double(station["gegrLat"] as! String)!
                    let longitude = Double(station["gegrLon"] as! String)!
                    result.append(Station(id: id, name: name, latitude: latitude, longitude: longitude))
                }
            }
        }
        return result
    }
    
    private func decodeSensorsResponse(_ data: Data) -> [Sensor]? {
        var result = [Sensor]()
        var sensorsArray: [Any]?
        do {
            sensorsArray = try JSONSerialization.jsonObject(with: data, options: []) as? [Any]
        } catch {
            print("🛑 - " + error.localizedDescription)
            return nil
        }
        if let sensorsArray = sensorsArray {
            for sensorsRaw in sensorsArray {
                if let sensor = sensorsRaw as? [String: Any], let param = sensor["param"] as? [String: Any] {
                    let id = sensor["id"] as! Int
                    let code = param["paramCode"] as! String
                    result.append(Sensor(id: id, code: code))
                }
            }
        }
        return result
    }
    
    private func decodeSensorValue(_ data: Data) -> (Double, Date)? {
        var respDict: [String: Any]?
        do {
            respDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print("🛑 - " + error.localizedDescription)
            return nil
        }
        if let respDict = respDict, let values = respDict["values"] as? [[String: Any]] {
            for value in values {
                if let val = value["value"] as? Double {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    formatter.isLenient = false
                    formatter.timeZone = TimeZone(abbreviation: "CET")
                    let date = decodeJSONDate(value["date"] as! String)
                    return (val, date)
                }
            }
        }
        return nil
    }
    
    private func decodeAQIName(_ data: Data) -> (String, Date)? {
        var respDict: [String: Any]?
        do {
            respDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print("🛑 - " + error.localizedDescription)
            return nil
        }
        if let respDict = respDict, let indexLevel = respDict["stIndexLevel"] as? [String: Any] {
            let indexLevelName = indexLevel["indexLevelName"] as! String
            let date = decodeJSONDate(respDict["stCalcDate"] as! String)
            return (indexLevelName.lowercased(), date)
        }
        return nil
    }
    
    private func decodeJSONDate(_ json: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.isLenient = false
        formatter.timeZone = TimeZone(abbreviation: "CET")
        return formatter.date(from: json)!
    }
}
