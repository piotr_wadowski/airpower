//
//  MainViewController.swift
//  AirPower
//
//  Created by Piotr Wadowski on 10.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit
import CoreLocation

class MainViewController: UIViewController, StationChooser {

    private struct MainVCConstants {
        
        static let wasFirstRunIdentifier = "wasFirstRun"
        static let citiesCellNibName = "CityTableViewCell"
        static let cityCellIdentifier = "cityCell"
        static let showStartupSegueIdentifier = "showStartup"
        static let showChooseStationsSegueIdentifier = "showChooseStation"
        static let showStationSegueIdentifier = "showStation"
        
        private init() { }
    }
    
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var aqiView: GaugeView!
    @IBOutlet private weak var aqiLabel: UILabel!
    @IBOutlet private weak var waitIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var citiesTableView: UITableView!
    
    private let dbAccess = DataAccessLocal()
    private let apiAccess = DataAccessAPI()
    private let locationManager = CLLocationManager()
    private var headerStation: Station?
    private var chosenStations = [Station]()
    private var chosenStation: Station?
    private var chosenAqi: AQILevel?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (!UserDefaults.standard.bool(forKey: MainVCConstants.wasFirstRunIdentifier)) {
            UserDefaults.standard.set(true, forKey: MainVCConstants.wasFirstRunIdentifier)
            self.performSegue(withIdentifier: MainVCConstants.showStartupSegueIdentifier, sender: self)
        }
        
        chosenStations = dbAccess.chosenStations
        
        locationManager.delegate = self
        
        prepareNavbar()
        prepareHeaderView()
        prepareCitiesTable()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deviceDidRotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    //MARK: - Public methods for navigation
    
    func backFromStartup() {
        refreshData()
    }
    
    func backFromChoosing() {
        refreshData()
    }
    
    //MARK: - Private methods
    
    private func prepareNavbar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshData))
        let stationsTitle = NSLocalizedString("STATIONS_TITLE", comment: "Title of button for showing stations to choose")
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: stationsTitle, style: .plain, target: self, action: #selector(showStationsController))
        
        let navTitleLabel = UILabel()
        navTitleLabel.textAlignment = .center
        navTitleLabel.numberOfLines = 1
        navTitleLabel.attributedText = Theme.current.logoText
        navigationItem.titleView = navTitleLabel
    }
    
    private func prepareHeaderView() {
        headerView.backgroundColor = Theme.current.backgroundColor
        aqiView.backgroundColor = Theme.current.backgroundColor
        cityLabel.font = Theme.current.infoLabelFont
        aqiLabel.font = Theme.current.infoLabelFont
        waitIndicator.color = Theme.current.mainColor
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(showGaugeStation))
        headerView.addGestureRecognizer(tapRecognizer)
        hideHeaderView()
        refreshHeaderView()
    }
    
    private func prepareCitiesTable() {
        citiesTableView.register(UINib(nibName: MainVCConstants.citiesCellNibName, bundle: Bundle.main), forCellReuseIdentifier: MainVCConstants.cityCellIdentifier)
        citiesTableView.rowHeight = 75.0
        citiesTableView.dataSource = self
        citiesTableView.delegate = self
    }
    
    private func refreshHeaderView() {
        waitIndicator.startAnimating()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestLocation()
        } else if let favStation = dbAccess.favStation {
            headerStation = favStation
            setHeaderView()
        } else {
            hideHeaderView()
        }
    }
    
    private func setHeaderView() {
        guard let headerStation = headerStation else { return }
        
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            hideHeaderView()
            return
        }
        
        if headerView.isHidden {
            headerView.isHidden = false
        }
        cityLabel.text = headerStation.name
        apiAccess.getAQIForStation(headerStation) { (resp) in
            if let (aqi, _) = resp {
                self.waitIndicator.stopAnimating()
                self.aqiView.percent = aqi.unhealhtyPercent
                self.aqiLabel.text = aqi.name
            } else {
                self.hideHeaderView()
            }
        }
    }
    
    private func hideHeaderView() {
        headerView.isHidden = true
    }
    
    @objc private func showStationsController() {
        self.performSegue(withIdentifier: MainVCConstants.showChooseStationsSegueIdentifier, sender: self)
    }
    
    @objc private func showGaugeStation() {
        chosenStation = headerStation
        self.performSegue(withIdentifier: MainVCConstants.showStationSegueIdentifier, sender: self)
    }
    
    @objc private func refreshData() {
        refreshHeaderView()
        chosenStations = dbAccess.chosenStations
        citiesTableView.reloadData()
    }
    
    @objc private func deviceDidRotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            hideHeaderView()
        } else if UIDevice.current.orientation == .portraitUpsideDown {
            return
        } else {
            setHeaderView()
        }
    }
    
    //MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MainVCConstants.showStartupSegueIdentifier, let dst = segue.destination as? StartupViewController {
            dst.sourceVC = self
        } else if segue.identifier == MainVCConstants.showChooseStationsSegueIdentifier, let nav = segue.destination as? UINavigationController, let dst = nav.topViewController as? ChooseStationViewController {
            dst.sourceVC = self
        } else if segue.identifier == MainVCConstants.showStationSegueIdentifier, let dst = segue.destination as? StationViewController {
            dst.station = chosenStation
            dst.lastAqi = chosenAqi
        }
    }

}

extension MainViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if let closestStation = dbAccess.getClosestStation(fromLocation: location) {
                headerStation = closestStation
                setHeaderView()
            } else {
                hideHeaderView()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("🛑 - Location manager failed: " + error.localizedDescription)
        hideHeaderView()
    }
    
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenStations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MainVCConstants.cityCellIdentifier, for: indexPath) as! CityTableViewCell
        
        let station = chosenStations[indexPath.row]
        cell.cityName = station.name
        apiAccess.getAQIForStation(station) { (resp) in
            if let (aqi, date) = resp {
                cell.aqi = aqi
                cell.date = date
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let station = chosenStations[indexPath.row]
        
        let favAction = UIContextualAction(style: .normal, title: nil) { (action, view, completion) in
            self.dbAccess.setFavourite(station: station, favourite: !station.isFavourite)
            completion(true)
        }
        favAction.backgroundColor = UIColor.orange
        favAction.image = station.isFavourite ? UIImage(named: "favStarEmpty") : UIImage(named: "favStar")
        
        return UISwipeActionsConfiguration(actions: [favAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? CityTableViewCell {
            chosenAqi = cell.aqi
        }
        chosenStation = chosenStations[indexPath.row]
        self.performSegue(withIdentifier: MainVCConstants.showStationSegueIdentifier, sender: self)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}
