//
//  CityTableViewCell.swift
//  AirPower
//
//  Created by Piotr Wadowski on 11.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet private weak var cityNameLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var aqiImageView: UIImageView!
    
    var cityName: String? {
        didSet {
            if let cityName = cityName {
                cityNameLabel.text = cityName
            } else {
                cityNameLabel.text = "-"
            }
        }
    }
    
    var date: Date? {
        didSet {
            if let date = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd.MM.yyyy HH:mm"
                formatter.locale = Locale(identifier: "pl_PL")
                dateLabel.text = formatter.string(from: date)
            } else {
                dateLabel.text = ""
            }
        }
    }
    
    var aqi: AQILevel? {
        didSet {
            if let aqi = aqi {
                aqiImageView.image = aqi.image
            } else {
                aqiImageView.image = nil
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        aqiImageView.contentMode = .scaleAspectFit
        cityNameLabel.font = Theme.current.infoLabelFont
    }
    
}
