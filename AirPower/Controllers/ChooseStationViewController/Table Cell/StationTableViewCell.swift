//
//  StationTableViewCell.swift
//  AirPower
//
//  Created by Piotr Wadowski on 11.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var ticImageView: UIImageView!
    
    var stationName: String? {
        didSet {
            if let stationName = stationName {
                nameLabel.text = stationName
            } else {
                nameLabel.text = "-"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        ticImageView.image = selected ? UIImage(named: "tic") : nil
    }
    
}
