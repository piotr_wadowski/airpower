//
//  ChooseStationViewController.swift
//  AirPower
//
//  Created by Piotr Wadowski on 11.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class ChooseStationViewController: UIViewController {

    private struct ChooseStationVCConstants {
        
        static let tableCellNibName = "StationTableViewCell"
        static let tableCellIdentifier = "stationCell"
        
        private init() { }
    }
    
    @IBOutlet private weak var stationsTableView: UITableView!
    
    private let dbAccess = DataAccessLocal()
    private var stations = [Station]()
    
    var sourceVC: StationChooser?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        stations = dbAccess.allStations
        prepareNavbar()
        prepareTableView()
    }
    
    //MARK: - Private methods
    
    private func prepareNavbar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideController))
    }
    
    private func prepareTableView() {
        stationsTableView.register(UINib(nibName: ChooseStationVCConstants.tableCellNibName, bundle: Bundle.main), forCellReuseIdentifier: ChooseStationVCConstants.tableCellIdentifier)
        stationsTableView.rowHeight = 50.0
        stationsTableView.allowsMultipleSelection = true
        stationsTableView.dataSource = self
        stationsTableView.delegate = self
        
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 50.0))
        searchBar.delegate = self
        searchBar.isTranslucent = false
        searchBar.barTintColor = Theme.current.mainColor
        searchBar.tintColor = UIColor.white
        searchBar.subviews[0].subviews.flatMap() { $0 as? UITextField }.first?.tintColor = UIColor.black
        stationsTableView.tableHeaderView = searchBar
        
        reselectStations()
    }
    
    private func reselectStations() {
        for (idx, station) in stations.enumerated() where station.isChosen {
            let indexPath = IndexPath(row: idx, section: 0)
            stationsTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }

    @objc private func hideController() {
        if let src = sourceVC {
            src.backFromChoosing()
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChooseStationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChooseStationVCConstants.tableCellIdentifier, for: indexPath) as! StationTableViewCell
        
        let station = stations[indexPath.row]
        cell.stationName = station.name
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dbAccess.setChosen(station: stations[indexPath.row], chosen: true)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        dbAccess.setChosen(station: stations[indexPath.row], chosen: false)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
}

extension ChooseStationViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            stations = dbAccess.allStations
        } else {
            stations = dbAccess.allStations.filter { $0.name.contains(searchText) }
        }
        stationsTableView.reloadData()
        reselectStations()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
