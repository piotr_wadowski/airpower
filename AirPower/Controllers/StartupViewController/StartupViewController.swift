//
//  StartupViewController.swift
//  AirPower
//
//  Created by Piotr Wadowski on 10.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit
import CoreLocation

class StartupViewController: UIViewController {

    @IBOutlet private weak var preparingView: UIView!
    @IBOutlet private weak var waitIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var waitMsgLabel: UILabel!
    @IBOutlet private weak var askLocationLabel: UILabel!
    @IBOutlet private weak var noButton: UIButton!
    @IBOutlet private weak var yesButton: UIButton!
    @IBOutlet private weak var endingView: UIView!
    @IBOutlet private weak var endingLabel: UILabel!
    @IBOutlet private weak var endingButton: UIButton!
    
    private let locationManager = CLLocationManager()
    
    var sourceVC: MainViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        preparePreparingView()
        downloadStations()
        prepareAskForLocationViews()
        prepareEndingView()
    }
    
    //MARK: - Private methods
    
    private func preparePreparingView() {
        waitIndicator.color = Theme.current.mainColor
        waitIndicator.startAnimating()
        waitMsgLabel.numberOfLines = 0
        waitMsgLabel.font = Theme.current.infoLabelFont
        let infoText = NSLocalizedString("WAITINFO_LABEL", comment: "Text for wait info label")
        waitMsgLabel.text = infoText
    }
    
    private func downloadStations() {
        let apiAccess = DataAccessAPI()
        apiAccess.getAllStations { (stations) in
            let dbAccess = DataAccessLocal()
            dbAccess.addStations(stations)
            for station in stations {
                apiAccess.getSensorsForStation(station, completion: { (sensors) in
                    dbAccess.addSensorsToStation(station, sensors: sensors)
                })
            }
            self.waitIndicator.stopAnimating()
            UIView.animate(withDuration: 0.5, animations: {
                self.preparingView.alpha = 0.0
            }, completion: { (_) in
                self.preparingView.isHidden = true
            })
        }
    }
    
    private func prepareAskForLocationViews() {
        askLocationLabel.font = Theme.current.infoLabelFont
        askLocationLabel.numberOfLines = 0
        let questionText = NSLocalizedString("USINGLOC_LABEL", comment: "Text for question about using location")
        askLocationLabel.text = questionText
        
        let noTitle = NSAttributedString(string: NSLocalizedString("NO", comment: "Text for no"), attributes: Theme.current.buttonsTitleAttributes)
        noButton.setAttributedTitle(noTitle, for: .normal)
        noButton.backgroundColor = Theme.current.mainColor
        noButton.layer.cornerRadius = noButton.frame.height / 2.0
        noButton.addTarget(self, action: #selector(showEndingView), for: .touchDown)
        
        let yesTitle = NSAttributedString(string: NSLocalizedString("YES", comment: "Text for yes"), attributes: Theme.current.buttonsTitleAttributes)
        yesButton.setAttributedTitle(yesTitle, for: .normal)
        yesButton.backgroundColor = Theme.current.mainColor
        yesButton.layer.cornerRadius = noButton.frame.height / 2.0
        yesButton.addTarget(self, action: #selector(askForGrantLocation), for: .touchDown)
    }
    
    private func prepareEndingView() {
        endingView.isHidden = true
        endingView.alpha = 0.0
        
        let welcomeText = NSLocalizedString("WELCOME_LABEL", comment: "Text welcome label")
        endingLabel.text = welcomeText
        endingLabel.font = Theme.current.infoLabelFont
        endingLabel.numberOfLines = 0
        
        let doneTitle = NSAttributedString(string: NSLocalizedString("DONE", comment: "Text for done"), attributes: Theme.current.buttonsTitleAttributes)
        endingButton.setAttributedTitle(doneTitle, for: .normal)
        endingButton.backgroundColor = Theme.current.mainColor
        endingButton.layer.cornerRadius = endingButton.frame.height / 2.0
        endingButton.addTarget(self, action: #selector(hideController), for: .touchDown)
    }
    
    @objc private func askForGrantLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        showEndingView()
    }
    
    @objc private func showEndingView() {
        endingView.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.endingView.alpha = 1.0
        }
    }
    
    @objc private func hideController() {
        if let src = sourceVC {
            src.backFromStartup()
        }
        self.dismiss(animated: true, completion: nil)
    }

}
