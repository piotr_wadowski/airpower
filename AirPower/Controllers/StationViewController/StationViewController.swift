//
//  PlaceViewController.swift
//  AirPower
//
//  Created by Piotr Wadowski on 12.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class StationViewController: UIViewController {

    private struct StationVCConstants {
        
        static let sensorCellNibName = "SensorCollectionViewCell"
        static let sensorCellIdentifier = "sensorCell"
        
        private init() { }
    }
    
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var aqiView: GaugeView!
    @IBOutlet private weak var aqiLabel: UILabel!
    @IBOutlet private weak var sensorsCollectionView: UICollectionView!
    
    private let apiAccess = DataAccessAPI()
    private let dbAccess = DataAccessLocal()
    private var sensors = [Sensor]()
    
    var station: Station!
    var lastAqi: AQILevel?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard station != nil else { self.dismiss(animated: true, completion: nil); return }
        
        sensors = Array(station.sensors)
        
        prepareNavbar()
        prepareHeaderView()
        prepareSensorsView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(deviceDidRotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    //MARK: - Private methods
    
    private func prepareNavbar() {
        navigationItem.title = station.name
    }
    
    private func prepareHeaderView() {
        headerView.backgroundColor = Theme.current.backgroundColor
        aqiView.backgroundColor = Theme.current.backgroundColor
        aqiLabel.font = Theme.current.infoLabelFont
        setHeaderView()
    }
    
    private func prepareSensorsView() {
        sensorsCollectionView.register(UINib(nibName: StationVCConstants.sensorCellNibName, bundle: Bundle.main), forCellWithReuseIdentifier: StationVCConstants.sensorCellIdentifier)
        sensorsCollectionView.dataSource = self
        sensorsCollectionView.delegate = self
        sensorsCollectionView.contentInset.left = 8.0
        sensorsCollectionView.contentInset.right = 8.0
        sensorsCollectionView.contentInset.top = 8.0
    }
    
    private func setHeaderView() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            hideHeaderView()
            return
        }
        
        if let lastAqi = lastAqi {
            aqiView.percent = lastAqi.unhealhtyPercent
            aqiLabel.text = lastAqi.name
            self.headerView.isHidden = false
        } else {
            apiAccess.getAQIForStation(station, completion: { (resp) in
                if let (aqi, _) = resp {
                    self.aqiView.percent = aqi.unhealhtyPercent
                    self.aqiLabel.text = aqi.name
                    self.headerView.isHidden = false
                } else {
                    self.headerView.isHidden = true
                }
            })
        }
    }
    
    private func hideHeaderView() {
        headerView.isHidden = true
    }
    
    @objc private func deviceDidRotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            hideHeaderView()
        } else if UIDevice.current.orientation == .portraitUpsideDown {
            return
        } else {
            setHeaderView()
        }
    }
}

extension StationViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sensors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StationVCConstants.sensorCellIdentifier, for: indexPath) as! SensorCollectionViewCell
        
        let sensor = sensors[indexPath.row]
        cell.name = sensor.code
        apiAccess.getActualValueForSensor(sensor) { (resp) in
            if let (value, _) = resp {
                cell.value = value
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100.0, height: 100.0)
    }
}
