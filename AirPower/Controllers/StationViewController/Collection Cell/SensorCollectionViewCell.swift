//
//  SensorCollectionViewCell.swift
//  AirPower
//
//  Created by Piotr Wadowski on 12.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

class SensorCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var valueLabel: UILabel!
    @IBOutlet private weak var unitLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    var value: Double? {
        didSet {
            if let value = value {
                valueLabel.text = String(format: "%.2f", arguments: [value])
            } else {
                valueLabel.text = "-.-"
            }
        }
    }
    
    var name: String? {
        didSet {
            if let name = name {
                nameLabel.text = name
            } else {
                valueLabel.text = "-"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = self.frame.width / 4.0
        
        self.backgroundColor = Theme.current.mainColor
        self.valueLabel.textAlignment = .center
        self.valueLabel.numberOfLines = 1
        self.valueLabel.textColor = Theme.current.itemsColor
        self.valueLabel.adjustsFontSizeToFitWidth = true
        self.unitLabel.textAlignment = .center
        self.unitLabel.numberOfLines = 1
        self.unitLabel.text = "μg/m\u{00b3}"
        self.nameLabel.font = Theme.current.infoLabelFont
        self.nameLabel.textAlignment = .center
        self.nameLabel.numberOfLines = 1
        self.nameLabel.textColor = Theme.current.itemsColor
    }

}
