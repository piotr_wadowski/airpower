//
//  StationChooser.swift
//  AirPower
//
//  Created by Piotr Wadowski on 12.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import Foundation

protocol StationChooser {
    
    func backFromChoosing()
    
}
