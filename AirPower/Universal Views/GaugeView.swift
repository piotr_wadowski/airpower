//
//  GaugeView.swift
//  AirPower
//
//  Created by Piotr Wadowski on 10.01.2018.
//  Copyright © 2018 Piotr Wadowski. All rights reserved.
//

import UIKit

//@IBDesignable
class GaugeView: UIView {
    
    var percent: Double = 0.0 {
        didSet {
            if percent > 1.0 {
                percent = 1.0
            }
            if percent < 0.0 {
                percent = 0.0
            }
            rotatePointer()
        }
    }

    private var gaugeWidth: CGFloat {
        return bounds.height / 4.0
    }
    private let margin: CGFloat = 30.0
    
    let pointerLayer = CAShapeLayer()
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        layer.addSublayer(pointerLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = createPointerPath()
        pointerLayer.path = path.cgPath
        pointerLayer.fillColor = Theme.current.mainColor.cgColor
        pointerLayer.bounds = path.bounds
        pointerLayer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
        pointerLayer.position = CGPoint(x: bounds.midX, y: bounds.height - margin)
    }
    
    override func draw(_ rect: CGRect) {
        let arcCenter = CGPoint(x: rect.midX, y: rect.height-margin)
        let radius = rect.height - gaugeWidth/2 - margin*2
        let startAngle: CGFloat = CGFloat(Double.pi)
        let endAngle: CGFloat = 0.0
        
        let path = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        path.lineWidth = gaugeWidth
        
        let newCGPath = path.cgPath.copy(strokingWithWidth: path.lineWidth, lineCap: path.lineCapStyle, lineJoin: path.lineJoinStyle, miterLimit: path.miterLimit)
        
        let newPath = UIBezierPath(cgPath: newCGPath)
        UIColor(named: "moderateColor")!.setFill()
        newPath.fill()
        newPath.addClip()
        
        drawGradient(startColor: UIColor(named: "goodColor")!, endColor: UIColor(named: "moderateColor")!, startPoint: CGPoint(x: 0.0, y: rect.height), endPoint: CGPoint(x: rect.midX, y: rect.midY))
        drawGradient(startColor: UIColor(named: "moderateColor")!, endColor: UIColor(named: "badColor")!, startPoint: CGPoint(x: rect.midX, y: rect.midY), endPoint: CGPoint(x: rect.width, y: rect.height))
    }
    
    private func createPointerPath() -> UIBezierPath {
        let pointerHeight = bounds.height - gaugeWidth - margin
        let pointerWidth: CGFloat = 18.0
        
        let pointerPath = UIBezierPath()
        pointerPath.move(to: CGPoint(x: pointerWidth / 2.0, y: 0.0))
        pointerPath.addCurve(to: CGPoint(x: pointerWidth / 2.0, y: pointerHeight), controlPoint1: CGPoint(x: pointerWidth / 2.0, y: 0.0), controlPoint2: CGPoint(x: -pointerWidth, y: pointerHeight))
        pointerPath.addCurve(to: CGPoint(x: pointerWidth / 2.0, y: 0.0), controlPoint1: CGPoint(x: pointerWidth*2.0, y: pointerHeight), controlPoint2: CGPoint(x: pointerWidth/2.0, y: 0.0))
        
        return pointerPath
    }
    
    private func rotatePointer() {
        let pi = CGFloat(Double.pi)
        let transform = CGAffineTransform(rotationAngle: -pi/2.0 + pi*CGFloat(percent))
        pointerLayer.setAffineTransform(transform)
    }

}
